<h1 align="center">Rylo Journal</h1>
Rylo is a journal app that helps you capture your moments, thoughts and share it with your friends.


## Built using a Modern stack 💎

### Frontend

- Typescript
- React with functional components and hooks
- Material-UI styled Components 
- Unit tests with React Testing Library
- Integration tests with Cypress

### Backend 

- Django REST framework
- Django ORM for interacting with the database
- PostgreSQL and
- Unit tests with Pytest

### Cloud based 

- Hosted on Firebase  
- Synced with Firestore so you can access your files anywhere super fast.
- Google analysis 

### Under development

- Create your journal your own style 
- Rooms - A place to chat with your friends
- Explore page for trending journals and collections


## Features ✨

- Multiple Collections boards
- Drag & drop posts
- tag your posts to find them easily later
- Edit Posts descriptions with Markdown
- Manage collaborators
- Update your profile & pick an avatar

## Installation 🛠

Project consist of 2 parts: Django backend and the React Typescript UI 

### Django

The application is built with Django. To run or build the app yourself, you'll need to have python installed.

```sh
cd backend

# mac and linux
python3 -m venv .venv
source .venv/bin/activate

# windows 
py -m venv env
.\env\scripts\activate

# install dependencies
pip install -r requirements/local.txt

# start server
python manage.py migrate
python manage.py createsuperuser --username admin --email a@a.com
python manage.py loaddata avatars
python manage.py runserver
```

The Django API is now accessible at `http://localhost:8000/api/` and 
the admin backend available at `http://localhost:8000/backdoor/`

### React 

For React UI you must have [Node.js](https://nodejs.org) v12 or greater 

```sh
cd frontend

# install packages
npm install

# run UI
npm start 
```
React app is now accessible at `http://localhost:3000`

Note: your app is the react URL. Django API is only for admin tasks and etc.

### Quality tools

Unit test for django and react 

Run Jest tests
```sh
npm test
```

Run Cypress tests
```sh
npm cypress run
```

#### Run Python tests

for running pytest you should comment firebase function signals at the end of the `backend/accounts/api.py`

(because it tries to run test on it too and will get error)
```sh
python -m pytest
```

Check formatting with Black for python
```sh
black --exclude env 
```

## Contact us
If you have any questions or suggestions we'd like to hear from you.
Leave us a note [Here](https://rylojournal.ir/contact) 

## License
[Parsa Mazaheri](https://www.linkedin.com/in/parsa-mazaheri/) & Mahdi Valadan.

