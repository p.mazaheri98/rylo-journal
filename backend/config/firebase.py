import os
import random
from datetime import datetime

import firebase_admin
from firebase_admin import credentials, firestore, storage, auth
from google.cloud.firestore_v1 import ArrayUnion

path = os.path.dirname(os.path.abspath(__file__))

if not firebase_admin._apps:
    cred = credentials.Certificate(path + "//rylojournal-firebase-adminsdk.json")
    firebase_admin.initialize_app(cred, {
        'projectId': 'rylojournal',
        'databaseURL': 'https://rylojournal.firebaseio.com/',  # database
        'storageBucket': 'rylojournal.appspot.com'  # storage for files
    })

db = firestore.client()
bucket = storage.bucket()


def create_user(name, email, username, password):
    user = auth.create_user(
        uid=username,
        email=email,
        email_verified=False,
        password=password,
        display_name=name)

    link = auth.generate_email_verification_link(email)
    print('Sucessfully created new user: {0}'.format(user.uid))
    return link


def authenticate(username, password):
    token = auth.create_custom_token(username)
    auth.verify_id_token(token)
    auth.get_user(username)


def delete_user(username):
    auth.delete_user(username)
    print('Sucessfully deleted user: {0}'.format(username))


def get_user(username):
    return auth.get_user(username)


def change_password(username, new_password):
    user = auth.update_user(username, password=new_password)
    print('password changed')


def update_info(username, field, value):
    if field == 'name':
        auth.update_user(username, display_name=value)
    elif field == 'email':
        auth.update_user(username, email=value)
    elif field == 'phone':
        auth.update_user(username, phone_number=value)
    if field == 'profile-pic':
        auth.update_user(username, photo_url=value)
    if field == 'username':
        auth.update_user(username, uid=value)


def update_all_info(updated_user):
    username = updated_user.username
    auth.update_user(username, display_name=updated_user.first_name + " " + updated_user.last_name)
    auth.update_user(username, email=updated_user.email)

# update_all_info('lana_r', 'username', 'lana_rd')


def reset_password(email):
    try:
        user = auth.get_user_by_email(email)
        return auth.generate_password_reset_link(email)
    except:
        return False


# print(reset_password('lana@gmail.com'))


def get_all_users():
    return auth.list_users().users


def is_username_taken(username):
    user = auth.get_user(username)
    if user is not None:
        return True
    return False


def is_user_veried(username):
    user = auth.get_user(username)
    return user.email_verified


"""
===================================================
                Database functions
===================================================
"""


def get_all_users_db():
    users_ref = db.collection(u'users')
    docs = users_ref.stream()

    for doc in docs:
        if doc.id == id:
            print(f'{doc.id} => {doc.to_dict()}')
            return doc.to_dict()


def add_user_to_db(name, email, username, password):
    doc_ref = db.collection(u'users').document(username)
    doc_ref.set({
        u'name': name,
        u'email': email,
        u'username': username,
        u'password': password
    })


def change_password_db(username, new_password):
    doc_ref = db.collection(u'users').document(username)
    doc_ref.set({
        u'password': new_password
    })


def get_user_db(username):
    doc_ref = db.collection(u'users').document(username)

    doc = doc_ref.get()
    if doc.exists:
        print(f'user data: {doc.to_dict()}')
        return doc.to_dict()
    else:
        print(u'No such user!')
        return None


def is_username_taken_db(username):
    docs = db.collection(u'users').where(u'username', u'==', username).stream()
    if docs is not None:
        return True
    return False


def get_specific_users_db():
    # Note: Use of CollectionRef stream() is prefered to get()
    docs = db.collection(u'users').where(u'capital', u'==', True).stream()

    for doc in docs:
        print(f'{doc.id} => {doc.to_dict()}')


def create_journal(username, name):
    id = str(random.randint(10000000000, 99999999999))
    doc_ref = db.collection(u'journals').document(id)
    doc_ref.set({
        u'name': name,
        u'owner': username,
        u'date_created': datetime.now(),
        u'can_edit': [username],
        u'cover': '',
        u'files': []
    })
    return id


def get_journal(username, uid):
    doc = db.collection(u'journals').document(uid).get()
    if doc.exists and username in doc.to_dict()['can_edit']:
        print(f'user data: {doc.to_dict()}')
        return doc.to_dict()
    else:
        return None


def edit_journal(username, uid, mode, value):
    doc_ref = db.collection(u'journals').document(uid)
    doc = doc_ref.get()
    if doc.exists and username in doc.to_dict()['can_edit']:
        if mode == 'name':
            doc_ref.update({u'name': value})
        elif mode == 'cover':
            doc_ref.update({u'cover': value})
        elif mode == 'files':
            doc_ref.update({u'name': ArrayUnion([value])})
        elif mode == 'can_edit':
            doc_ref.update({u'can_edit': ArrayUnion([value])})
        print('successfully edited')


def add_contact_form(name, email, subject, message):
    id = subject + " " + str(random.randint(10000000, 99999999))
    doc_ref = db.collection(u'contact_us').document(id)
    doc_ref.set({
        u'name': name,
        u'email': email,
        u'subject': subject,
        u'message': message,
        u'date_created': datetime.now(),
    })


# add_contact_form('mehdi valadan', 'm_zoj@yahoo.com', 'recom', 'parsa is very goood ')

# print(create_journal('p.mazaheri', 'Travel'))
# edit_journal('p.mazaheri', '77740385439', 'can_edit', 'd.kntu')


"""
===================================================
                Storage functions
===================================================
"""


def upload_file(file, cloud_address):
    blob = bucket.blob(cloud_address)
    blob.upload_from_filename(file)
    print("File {} uploaded to {}.".format(file, cloud_address))


def download_file(cloud_address, file):
    blob = bucket.blob(cloud_address)
    if blob.exists():
        blob.download_to_filename(file)
        print("File {} downloaded from {}.".format(file, cloud_address))


def get_files_link(username, collection_name):
    blob = bucket.blob('collections/' + username + collection_name)
    if blob.exists():
        blob.download_as_string()
    print("File {} downloaded from {}.".format(collection_name, username))


# get_files_link('p.mazaheri', 'food')


def file_exist(cloud_address):
    blob = bucket.blob(cloud_address)
    if blob.exists():
        return True
    else:
        return False


def delete_file(cloud_address):
    blob = bucket.blob(cloud_address)
    if blob.exists():
        blob.delete()
        print("File {} deleted".format(cloud_address))


def find_file(cloud_address):
    # file name pattern e.g. files\pic  (pic.jpg, pic.png, ...)
    blob = bucket.list_blobs(prefix=cloud_address)
    files = []
    for b in blob:
        files.append(b)
    return files

# find_file('files/1')
# delete_file('files/parsa')
# get_file('files/parsa', 'juj.png')
# upload_file("../app/static/img/friends.png", "files/parsa")
# create_user('Ross Geller', 'dr_geller', 'qwerty1234')
# user = get_user('000000001')
