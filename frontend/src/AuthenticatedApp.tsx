import React from "react";
import { Switch, Route, RouteProps } from "react-router-dom";
import styled from "@emotion/styled";

import Board from "features/board";
import BoardList from "features/board/BoardList";
import Navbar from "components/Navbar";
import Home from "features/home/Home";
import BoardBar from "features/board/BoardBar";
import Profile from "features/profile/Profile";
import Sidebar from "features/sidebar/Sidebar";
import PageError from "components/PageError";
import { sidebarWidth } from "const";
import { useTheme, WithTheme } from "@material-ui/core";
// new pages
import About from "./features/about/About";
import ContactUs from "./features/contactUs/ContactUs";
import Blog from "./features/blog/Blog";
import Lottie from "react-lottie";
import animationData from "static/animations/404_not_found.json";
import ChangePasswordDialog from "./features/auth/ChnagePassword";

const Main = styled.div<WithTheme>`
  ${props => props.theme.breakpoints.up("sm")} {
    margin-left: ${sidebarWidth + 8}px;
  }
`;

const Wrapper: React.FC = ({ children }) => {
  const theme = useTheme();

  return (
    <>
      <Sidebar />
      <Main theme={theme}>
        <Navbar />
        {children}
      </Main>
    </>
  );
};

const AppRoute = (props: RouteProps) => (
  <Route {...props}>
    <Wrapper>{props.children}</Wrapper>
  </Route>
);

const AuthenticatedApp = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  const halfW = window.innerWidth / 2 - 80;

  return (
    <Switch>
      <AppRoute exact path="/about">
        <About />
      </AppRoute>
      <AppRoute exact path="/contactUs">
        <ContactUs />
      </AppRoute>
      <AppRoute exact path="/blog">
        <Blog />
      </AppRoute>
      <AppRoute exact path="/profile">
        <Profile />
      </AppRoute>
      <AppRoute exact path="/collections">
        <BoardList />
      </AppRoute>
      <AppRoute exact path="/forgot_password">
        <ChangePasswordDialog />
      </AppRoute>
      <AppRoute exact path="/b/:id">
        <BoardBar />
        <Board />
      </AppRoute>

      <AppRoute exact path="/">
        <Home />
      </AppRoute>
      <Route path="*">
        <Lottie options={defaultOptions} height={300} width={300} />
        <div
          style={{
            position: "absolute",
            top: 64,
            left: halfW
          }}
        >
          <PageError>Page not found.</PageError>
        </div>
      </Route>
    </Switch>
  );
};

export default AuthenticatedApp;
