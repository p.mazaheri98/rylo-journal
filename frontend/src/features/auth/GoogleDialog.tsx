import React from "react";
import styled from "@emotion/styled";
import { useDispatch } from "react-redux";
import { login, register as registerUser } from "./AuthSlice";
import { GoogleLogin } from "react-google-login";

const FormActions = styled.div`
  margin-top: 1rem;
  text-align: right;
`;

interface FormData {
  username: string;
  password: string;
}

const GoogleDialog = () => {
  const dispatch = useDispatch();

  const responseGoogle = response => {
    const username = response.profileObj.email.match("^.*(?=@)");
    const password = "plTi8ughw3r1Viol5Mn7bVCrKx";
    const fields = {
      email: response.profileObj.email,
      username: username,
      password1: password,
      password2: password
    };
    alert("Temporary unavailable. Please sign up using email");
    // dispatch(registerUser(fields));
    // dispatch(login({ username, password }));
  };

  return (
    <>
      <GoogleLogin
        clientId="49632191115-c04f9u6rkl5mv1aprmbfs6e7rt8c1trk.apps.googleusercontent.com"
        buttonText="Sign in with Google"
        // theme="dark"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={"single_host_origin"}
      />
    </>
  );
};

export default GoogleDialog;
