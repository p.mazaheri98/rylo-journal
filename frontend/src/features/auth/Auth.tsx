import React from "react";
import styled from "@emotion/styled";
import LoginDialog from "./LoginDialog";
import RegisterDialog from "./RegisterDialog";
import GoogleDialog from "./GoogleDialog";
import EnterAsGuest from "./EnterAsGuest";
import Footer from "./Footer";
import Logo from "../../static/svg/rylo-animated.svg";
import Logo2 from "../../static/svg/journal-animated.svg";

const Container = styled.div`
  margin-top: 20vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Title = styled.h1`
  margin-top: 0;
  margin-bottom: 0.75rem;
`;

const Auth = () => {
  return (
    <Container>
      <div>
        <img src={Logo} />
        <img src={Logo2} />
      </div>
      {/* <Title>Rylo Journal</Title> */}
      <div>
        <LoginDialog />
        <RegisterDialog />
      </div>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <div>
        <GoogleDialog />
      </div>
      <EnterAsGuest />
      <Footer />
    </Container>
  );
};

export default Auth;
