import React, { useState } from "react";
import { useForm } from "react-hook-form";
import {
  Button,
  TextField,
  Dialog,
  DialogContent,
  DialogTitle
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { css } from "@emotion/core";
import styled from "@emotion/styled";
import { clearErrors } from "./AuthSlice";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "store";
import Close from "components/Close";
import { createSuccessToast } from "../toast/ToastSlice";

const FormActions = styled.div`
  margin-top: 1rem;
  text-align: right;
`;

interface FormData {
  email: string;
}

const ForgotPasswordDialog = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors, setError } = useForm<FormData>();
  const apiErrors = useSelector(
    (state: RootState) => state.auth.registerErrors
  );
  const [open, setOpen] = useState(false);

  React.useEffect(() => {
    if (apiErrors) {
      for (const errorKey in apiErrors) {
        // @ts-ignore
        setError(errorKey, "api_error", apiErrors[errorKey]);
      }
    }
  }, [apiErrors]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    dispatch(clearErrors());
  };

  const onSubmit = handleSubmit(fields => {
    if (
      fields.email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)
    ) {
      dispatch(createSuccessToast("Reset Link sent."));
      window.location.reload();
      // dispatch(registerUser(fields));
    } else {
      alert("Enter a valid email address");
    }
  });

  return (
    <>
      <a
        css={css`
          text-decoration: none;
          color: blue;
          &:hover {
            color: #0747a6;
          }
        `}
        onClick={handleOpen}
      >
        Forgot Password ?
      </a>
      <Dialog
        open={open}
        onClose={handleClose}
        keepMounted={false}
        aria-labelledby="register-dialog-title"
        css={css`
          & .MuiDialog-paper {
            padding: 2rem 1.5rem;
          }
        `}
        maxWidth="xs"
        fullWidth
      >
        <Close onClose={handleClose} />
        <DialogTitle id="register-dialog-title">Forgot Password</DialogTitle>
        <form onSubmit={onSubmit}>
          <DialogContent>
            {apiErrors?.non_field_errors && (
              <Alert
                severity="error"
                css={css`
                  margin-bottom: 0.75rem;
                `}
              >
                {apiErrors.non_field_errors?.map(errorMsg => (
                  <div key={errorMsg}>{errorMsg}</div>
                ))}
              </Alert>
            )}
            Enter your email address
            <TextField
              id="email"
              name="email"
              margin="dense"
              label="Email"
              variant="outlined"
              inputRef={register({ required: "This field is required" })}
              helperText={errors.email?.message}
              error={Boolean(errors.email)}
              fullWidth
            />
            <FormActions>
              <Button variant="contained" color="primary" type="submit">
                Send Reset Link
              </Button>
            </FormActions>
          </DialogContent>
        </form>
      </Dialog>
    </>
  );
};

export default ForgotPasswordDialog;
