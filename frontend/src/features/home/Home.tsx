import React from "react";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import styled from "@emotion/styled";
import { css } from "@emotion/core";
import Lottie from "react-lottie";
import files from "../../static/animations.json";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  // font-family: "Pristina", serif;
`;
const PStyle = styled.div`
  @import url("https://fonts.googleapis.com/css2?family=Merienda&display=swap");
  font-family: "Merienda", serif;
  font-size: 22px;
`;

const HeroContainer = styled.div``;

const Home = () => {
  const i = Math.floor(Math.random() * Math.floor(11));
  const filename = files[i].filename;
  const text = files[i].text;
  const preposition = text.substr(0, text.indexOf(" "));
  const message = text.substr(text.indexOf(" ") + 1);
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const animationData = require("static/animations/homepage/" + filename);
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  return (
    <Container>
      <HeroContainer>
        <PStyle>
          <p>
            You can use it {preposition} <b> {message} </b>
          </p>
        </PStyle>
        <Lottie options={defaultOptions} height={300} width={300} />
      </HeroContainer>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <Link
        css={css`
          text-decoration: none;
          color: #333;
        `}
        to="/collections/"
      >
        <Button
          color="primary"
          variant="contained"
          style={{ textTransform: "none" }}
        >
          Get Started
        </Button>
      </Link>
    </Container>
  );
};

export default Home;
