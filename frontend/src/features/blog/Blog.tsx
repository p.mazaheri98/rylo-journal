/* eslint-disable react/jsx-no-undef */
import React from "react";
import { Container, Divider, useTheme, WithTheme } from "@material-ui/core";
import styled from "@emotion/styled";
import Lottie from "react-lottie";
import construction from "../../static/animations/construction-in-process.json";
import ryloStarted from "../../static/animations/confetti-falling.json";
import projectIdea from "../../static/animations/project-idea.json";

const Title = styled.h3`
  margin-top: 2rem;
  margin-bottom: 0.5rem;
  @import url("https://fonts.googleapis.com/css2?family=Merienda&display=swap");
  font-size: 18px;
`;
const StyleC = styled.div`
  font-family: "Script MT", serif;
  font-size: 14px;
`;
const StyleDate = styled.div`
  color: #6554c0;
  font-size: 12px;
`;
const FormContainer = styled.div<WithTheme>`
  margin-top: 2rem;
  display: flex;
  align-items: center;
  ${props => props.theme.breakpoints.down("xs")} {
    flex-direction: column;
  }
  font-size: 15px;
`;

const Fields = styled.div``;

const Row = styled.div``;

const Blog = () => {
  const theme = useTheme();

  return (
    <Container maxWidth="sm">
      <Title>Blog</Title>
      <Divider />
      <FormContainer theme={theme}>
        <Fields>
          <Row>
            <Lottie
              options={{
                loop: true,
                autoplay: true,
                animationData: ryloStarted,
                rendererSettings: {
                  preserveAspectRatio: "xMidYMid slice"
                }
              }}
              height={250}
              width={250}
            />
            <p>
              Rylo has been officially released. You can now access Rylo at{" "}
              <a href="https://rylojournal.ir"> Here </a>. Hope you guys enjoy
              it. 📜
            </p>
            <StyleC>by Parsa Mazaheri</StyleC>
            <StyleDate>July 16, 2020</StyleDate>
          </Row>
          <Row>
            <p>&nbsp;</p>
          </Row>
          <Row>
            <Lottie
              options={{
                loop: true,
                autoplay: true,
                animationData: construction,
                rendererSettings: {
                  preserveAspectRatio: "xMidYMid slice"
                }
              }}
              height={250}
              width={250}
            />
            <p>
              Rylo has been under development for almost a month now and in this
              time we tried to add some cool stuff to it and also do some behind
              the scene works.
            </p>
            <StyleC>by Mehdi Valadan</StyleC>
            <StyleDate>June 28, 2020</StyleDate>
          </Row>
          <Row>
            <p>&nbsp;</p>
          </Row>
          <Row>
            <Lottie
              options={{
                loop: true,
                autoplay: true,
                animationData: projectIdea,
                rendererSettings: {
                  preserveAspectRatio: "xMidYMid slice"
                }
              }}
              height={250}
              width={250}
            />
            <p>Today Rylo project has been officially started ! 🚀 </p>
            <StyleC>by Parsa Mazaheri</StyleC>
            <StyleDate>April 27, 2020</StyleDate>
          </Row>
          <Row>
            <p>&nbsp;</p>
          </Row>
        </Fields>
      </FormContainer>
    </Container>
  );
};

export default Blog;
