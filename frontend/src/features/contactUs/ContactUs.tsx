/* eslint-disable react/jsx-no-undef */
import React from "react";
import {
  Container,
  Divider,
  TextField,
  Button,
  useTheme,
  WithTheme
} from "@material-ui/core";
import styled from "@emotion/styled";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "store";
import { css } from "@emotion/core";
import { useForm } from "react-hook-form";
import { Alert, AlertTitle } from "@material-ui/lab";
import { createSuccessToast } from "../toast/ToastSlice";

const Title = styled.h3`
  margin-top: 2rem;
  margin-bottom: 0.5rem;
  font-size: 18px;
`;

const FormContainer = styled.div<WithTheme>`
  margin-top: 2rem;
  display: flex;
  align-items: center;
  ${props => props.theme.breakpoints.down("xs")} {
    flex-direction: column;
  }
`;

const Row = styled.div``;

const UserForm = styled.form`
  width: 100%;
`;

const Fields = styled.div``;

interface FormData {
  first_name: string;
  email: string;
  message: string;
  subject: string;
}

const ContactUs = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const userDetail = useSelector(
    (state: RootState) => state.profile.userDetail
  );
  const apiErrors = useSelector((state: RootState) => state.profile.apiErrors);
  const loading = useSelector((state: RootState) => state.profile.loading);
  const { register, errors, handleSubmit, setError } = useForm<FormData>({
    mode: "onChange"
  });

  React.useEffect(() => {
    // dispatch(fetchUserDetail());
  }, [userDetail?.id]);

  React.useEffect(() => {
    if (apiErrors) {
      for (const errorKey in apiErrors) {
        // @ts-ignore
        setError(errorKey, "api_error", apiErrors[errorKey]);
      }
    }
  }, [apiErrors]);

  if (!userDetail) {
    return null;
  }

  const onSubmit = async (data: FormData) => {
    dispatch(createSuccessToast("Your message has been sent"));
    window.location.reload();
  };

  return (
    <Container maxWidth="sm">
      {userDetail.is_guest && (
        <Alert
          severity="warning"
          variant="outlined"
          css={css`
            margin: 1rem 0;
          `}
        >
          <AlertTitle>Warning</AlertTitle>
          Guest accounts are deleted 24 hours after creation!
        </Alert>
      )}
      <Title>Contact Us</Title>
      <Divider />
      <FormContainer theme={theme}>
        {/*<UserAvatar />*/}
        <UserForm onSubmit={handleSubmit(onSubmit)}>
          <Fields>
            <Row>
              <TextField
                id="first_name"
                name="first_name"
                inputRef={register({ required: "This field is required" })}
                defaultValue={userDetail.first_name}
                label="First name"
                helperText={errors.first_name?.message}
                error={Boolean(errors.first_name)}
                variant="outlined"
                margin="dense"
                fullWidth
              />
            </Row>
            <Row>
              <TextField
                id="email"
                name="email"
                inputRef={register({
                  pattern: {
                    value: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    message: "Invalid email"
                  }
                })}
                defaultValue={userDetail.email}
                helperText={errors.email?.message}
                error={Boolean(errors.email)}
                label="Email"
                variant="outlined"
                margin="dense"
                fullWidth
              />
            </Row>
            <Row>
              <TextField
                id="subject"
                name="subject"
                inputRef={register({ required: "This field is required" })}
                // defaultValue={userDetail.first_name}
                // helperText={errors.first_name?.message}
                // error={Boolean(errors.first_name)}
                label="Subject"
                variant="outlined"
                margin="dense"
                fullWidth
              />
            </Row>
            <Row>
              <TextField
                id="message"
                name="message"
                inputRef={register({ required: "This field is required" })}
                // defaultValue={userDetail.first_name}
                // helperText={errors.first_name?.message}
                // error={Boolean(errors.first_name)}
                label="Message"
                rows={5}
                variant="outlined"
                margin="dense"
                fullWidth
                multiline={true}
              />
            </Row>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              disabled={loading}
              // data-testid="profile-save"
              css={css`
                margin: 1rem 0;
                text-align: right;
              `}
            >
              Submit
            </Button>
          </Fields>
        </UserForm>
      </FormContainer>
    </Container>
  );
};

export default ContactUs;
