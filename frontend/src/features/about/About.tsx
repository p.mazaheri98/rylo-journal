import React from "react";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import styled from "@emotion/styled";
import { ReactComponent as Hero } from "static/svg/collection.svg";
import { css } from "@emotion/core";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 25px;
  text-align: center;
  // font-size: 16px;
`;
const PStyle = styled.div`
  // font-family: "Script MT Bold", serif;
  font-size: 16px;
`;
const HStyle = styled.div`
  @import url("https://fonts.googleapis.com/css2?family=Merienda&display=swap");
  // font-family: "Comic Sans MS";
  font-family: "Merienda";  
  font-weight: bold
  font-size: 15px;
`;
const SStyle = styled.div`
  font-size: 18px;
  font-family: "Monotype Corsiva", serif;
`;

const HeroContainer = styled.div``;

const About = () => {
  return (
    <Container>
      <HeroContainer>
        <Hero width={260} height={170} />
      </HeroContainer>
      <div>
        <HStyle>
          <h1>Rylo is what happens around you</h1>
        </HStyle>
        <PStyle>
          <p>
            Rylo is a journal app that helps you capture your moments, thoughts
            and share it with your friends.
            <br />
            We made it really simple for people to make a collection of what
            they Love with the people they love together.
            <br />
            from organizing their work to having a personal diary or from
            planning for your weekend to your trip summery.
            <br />
            You can use Rylo the way you want.
          </p>
        </PStyle>
        <SStyle>
          <p>Mahdi Valadan & Parsa Mazeheri Project</p>
          <p> Special thanks to M.Nazari </p>
        </SStyle>
      </div>
    </Container>
  );
};

export default About;
