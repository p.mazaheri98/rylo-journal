import React from "react";
import { Drawer, List, Hidden, Tooltip } from "@material-ui/core";
import { css } from "@emotion/core";
import { sidebarWidth } from "const";
import styled from "@emotion/styled";
import Logo from "static/png/foxPurple.png";
import { NavLink, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  mobileDrawerOpen,
  setMobileDrawerOpen
} from "features/responsive/ResponsiveSlice";

const Container = styled.div`
  height: 100%;
  background-color: #666eee;
  background: linear-gradient(to bottom, #00b8d9, #79e2f2);
`;

const TopArea = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 1rem;
`;

const TopLinks = styled.div`
  font-size: 16px;
  margin-top: 40px;
  left: 0px;
  bottom: 2rem;
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const linkStyles = css`
  @import url("https://fonts.googleapis.com/css2?family=Merienda&display=swap");
  @import url("https://fonts.googleapis.com/css2?family=Acme&display=swap");
  display: block;
  //Colors
  // color: #8e97d8;
  // color: #0747a6;
  color: #403294;
  //
  font-weight: bold;
  //#Fonts
  // font-family: "Monotype Corsiva", serif;
  // font-family: "Comic Sans MS", serif;
  // font-family: "Pristina", serif;
  // font-family: "Merienda", cursive;
  font-family: "Acme";

  //#

  padding: 6px 20px;
  text-decoration: none;
  &:hover {
    color: #fff;
    cursor: pointer;
  }
  &.active {
    color: #fff;
  }
`;

const Sidebar = () => {
  const dispatch = useDispatch();
  const mobileOpen = useSelector(mobileDrawerOpen);

  const handleCloseMobileDrawer = () => {
    dispatch(setMobileDrawerOpen(false));
  };

  return (
    <>
      <Hidden smUp implementation="css">
        <Drawer
          variant="temporary"
          anchor="left"
          open={mobileOpen}
          onClose={handleCloseMobileDrawer}
          ModalProps={{ keepMounted: true }}
        >
          <DrawerContent />
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer anchor="left" variant="permanent">
          <DrawerContent />
        </Drawer>
      </Hidden>
    </>
  );
};

const BottomBlock = styled.div`
  position: absolute;
  left: 0px;
  bottom: 2rem;
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  //
  font-size: 14px;
  //
`;

const DrawerContent = () => {
  const history = useHistory();

  return (
    <Container>
      <TopArea>
        <a href={"/"}>
          <img src={Logo} height={74} />
        </a>
      </TopArea>
      {/*<List*/}
      {/*  css={css`*/}
      {/*    width: ${sidebarWidth}px;*/}
      {/*    margin-top: 40px;*/}
      {/*    align-item: center;*/}
      {/*  `}*/}
      {/*>*/}
      <TopLinks>
        <NavLink to="/" exact css={linkStyles}>
          Home
        </NavLink>
        <NavLink to="/collections/" exact css={linkStyles}>
          Collections
        </NavLink>
        <NavLink to="/profile/" exact css={linkStyles}>
          Profile
        </NavLink>
      </TopLinks>
      {/*</List>*/}
      <BottomBlock>
        <NavLink to="/about/" exact css={linkStyles}>
          About
        </NavLink>
        <NavLink to="/blog/" exact css={linkStyles}>
          Blog
        </NavLink>
        <NavLink to="/contactUs/" exact css={linkStyles}>
          Contact Us
        </NavLink>
      </BottomBlock>
    </Container>
  );
};

export default Sidebar;
