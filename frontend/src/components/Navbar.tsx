import React from "react";
import styled from "@emotion/styled";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { barHeight } from "const";
import UserMenu from "./UserMenu";
import {
  faRocket,
  faBars,
  faBook,
  faDharmachakra
} from "@fortawesome/free-solid-svg-icons";
import { faSnowflake } from "@fortawesome/free-regular-svg-icons";
import { setMobileDrawerOpen } from "features/responsive/ResponsiveSlice";
import { useDispatch, useSelector } from "react-redux";
import { Hidden } from "@material-ui/core";
import { RootState } from "../store";
import { fetchUserDetail } from "../features/profile/ProfileSlice";
import Logo from "../static/svg/rylo-header.svg";

const Container = styled.div`
  min-height: ${barHeight}px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: 0.5rem;
  padding-right: 0.5rem;
  border-bottom: 1px solid #999;
`;

const Item = styled.div`
  font-size: 1rem;
  color: #333;
`;

const Icons = styled.div`
  font-size: 1.25rem;
  a {
    color: #888;
    &:hover {
      color: #333;
    }
  }
  .active {
    color: #333;
  }
`;

const Navbar = () => {
  const userDetail = useSelector(
    (state: RootState) => state.profile.userDetail
  );
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(fetchUserDetail());
  }, [userDetail?.id]);

  return (
    <Container>
      <Item>
        <Icons>
          <Hidden smUp implementation="css">
            <FontAwesomeIcon
              icon={faBars}
              onClick={() => dispatch(setMobileDrawerOpen(true))}
            />
          </Hidden>
          <Hidden xsDown implementation="css">
            <FontAwesomeIcon icon={faSnowflake} />
          </Hidden>
        </Icons>
      </Item>
      <Item>
        <img src={Logo} height={56} />
      </Item>
      <Item>
        {userDetail?.first_name} &nbsp;&nbsp;
        <UserMenu />
        &nbsp;&nbsp;
      </Item>
    </Container>
  );
};

export default Navbar;
